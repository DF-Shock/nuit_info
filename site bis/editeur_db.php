<!DOCTYPE html>
<html>
<title>Admin Panel</title>
<link rel="stylesheet" href="css/style.css">
<body>

  <?php

    require 'veriflogin.php';
    if (VERIF() == 'False') { ?> <script> window.location.replace("../index.php"); </script> <?php }

    $servername = "LaBaseDeDonnée";
    $username = "Surnom";
    $password = "MDP";
    $dbname = "NomDeLaTable";

    try { $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password); } catch(Exception $e) { die('Erreur : '.$e->getMessage()); }
  ?>

  <div>
  		<div class="VT-inline">
        <form method="get" action='update.php'>
          <fieldset>
            <legend> Modifier </legend>
            <p>
              <select name="what" id="textfenwhat">
                <optgroup label="Quoi modifier">
                <?php $reponse = $conn->query('SELECT * FROM textfen');
                while ($donnees = $reponse->fetch()) { ?>
                  <option value="<?php echo $donnees['namevar']; ?>"><?php echo $donnees['namevar']; ?></option>
                <?php }
                $reponse->closeCursor(); ?>
              </select>
            </p>
            <textarea name="modif" style="width:200px; height:200px;"></textarea></br></br>
            <input type="submit" value="Envoyer" />
          </fieldset>
        </form>
        <form method="get" action="add.php">
          <fieldset>
            <legend> Ajouter </legend>
            <label for="text">ID :</label>
            <input type="text" name="id" id="id" /> <br /> <br />
            <label for="text">Namevar :</label>
            <input type="text" name="name" id="name" /> <br /> <br />
            <input type="submit" value="Envoyer" />
          </fieldset>
        </form>
        <form method="get" action="remove.php">
          <fieldset>
            <legend> Supprimer </legend>
            <label for="text">ID :</label>
            <input type="text" name="name" id="name" /> <br /> <br />
            <input type="submit" value="Envoyer" />
          </fieldset>
        </form>
  		</div>
  		<div class="VT-inline">
        <table id="tableau">
          <thead>
            <tr>
              <th scope="col" >ID</th>
              <th scope="col" >Namevar</th>
              <th scope="col" >Value</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $reponse = $conn->query('SELECT * FROM textfen');
            while ($donnees = $reponse->fetch())
            {
            ?>
            <tr>
              <td><?php echo urldecode($donnees['ID']); ?></td>
              <td><?php echo urldecode($donnees['namevar']); ?></td>
              <td><?php echo urldecode($donnees['value']); ?></td>
            </tr>
            <?php
            }
            $reponse->closeCursor(); ?>
          </tbody>
        </table>
  		</div>
  </div>

  <?php $conn->close(); ?>

</body>
</html>
