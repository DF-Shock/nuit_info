class Monde {
	constructor() {
		this.base = new Base();
		this.ia = new IA();
		this.tempete = false;
		this.jour = 0;
		this.termine=false;
	}

	message(texte) {
		document.getElementById("botText").innerHTML = texte;
	}

	message2(texte) {
		document.getElementById("chatText").innerHTML = texte;
	}

	message3(texte) {
		document.getElementById("chatText2").innerHTML = texte;
	}

	iAReaction(input) {
		//SALUT
		var salut  = ["bonjour", "salut", "coucou", "bonsoir"]
		var salutTrouve = false;
		for (var i=0; i<=salut.length && !salutTrouve; i++) {
			if (!salutTrouve && input.indexOf(salut[i])!=-1) {
				this.ia.karma+=10;
				salutTrouve=true;
			}
		}
		//Mot, Chacun compte
		var mot = ["ca va", "ça va", "merci", "s'il-te-plait", "s'il te plait", "paye"];
		for (var i=0; i<=mot.length; i++) {
			if (input.indexOf(mot[i])!=-1) {
				if (Math.floor(Math.random()*2)==0) {
					this.ia.karma+=10;
				}
				else {
					this.ia.karma+=-10;
				}
			}
		}
		//messsage
		if (this.ia.karma>50) {
			this.message("Oh! je suis contente!");
		}
		else {
			this.message("Bah! Tu m'a mit de mauvaise humeur.");
		}
	}

	update() {
		document.getElementById("nrj").innerHTML=monde.base.energie;
		document.getElementById("mtr").innerHTML=monde.base.materiel;
		document.getElementById("scrt").innerHTML=monde.base.securite;
		document.getElementById("eau").innerHTML=monde.base.eau;
		document.getElementById("drn").innerHTML=monde.base.drone;
		document.getElementById("com").innerHTML=monde.base.communication;
		document.getElementById("snt").innerHTML=monde.base.sante;
	}

	iAJoue() {
		var event = this.ia.selectEvenement();
		console.log(event);
		var executeEvent = (Math.floor(Math.random()*100)>=this.ia.karma); //exécute ssi random(0, 100)>=karma
		if (executeEvent) {
			this.message2("Prend ca!");
			this.base = this.base.evenement[event].agir(this.base);
			this.message3(this.base.evenement[event].nom);
			this.update();
		} else {
			this.message2("Non, pas cette fois");
		}
	}

	checkGame() {
		//déclenche la fin du jeu ssi un paramètre attein 0
		if (this.base.energie<=0 || this.base.materiel<=0 || this.base.securite<=0 || this.base.eau<=0 || this.base.alimentation<=0 || this.base.moral<=0 || this.base.sante<=0) {
			this.termine=true;
		}
	}

	joueurJoue() {
		if (!this.termine) {
			document.getElementById("menuJoueur").style.visibility="visible";
		}
	}

	go(input) {
		this.iAReaction(input);
		this.iAJoue();
		this.checkGame();
		this.joueurJoue();
		this.jour+=1;
		if (Math.floor(Math.random()*3)==0) {
			this.tempete=true;
			if (this.base.comunication==true) {
				console.log("TEMPETTE");
			}
		}
		this.ia.karma=50;//Reset du karma
	}
}