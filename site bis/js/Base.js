class Base {
	constructor() {
		this.param = ["energie", "materiel", "securite", "eau", "alimentation", "drone", "communication", "moral", "sante"];

		this.energie = 100;
		this.materiel = 100;
		this.securite = 100;
		this.eau = 100;
		this.alimentation = 100;
		this.drone = true;
		this.communication = true;
		this.moral = 100;
		this.sante = 100;

		this.evenement = [new Evenement("Panne de communication", [6, 2], [false, -20]),
			new Evenement("Surconsommation de l'energie", [0, 2], [-20, -20]),
			new Evenement("Panne de drone", [5], [false]),
			new Evenement("Mission", [0, 1, 3, 4], [-40, 30, 30, 30]),
			new Evenement("Accident de porte", [8], [-30]),
			new Evenement("Reparation", [3, 1, 4], [-25, -25, -25]),
			new Evenement("Fuite d'eau", [3], [-35])];
	}
	
}