Arena = function(game) {
    // Appel des variables nécéssaires
    this.game = game;
    var scene = game.scene;

    var materialGround = new BABYLON.StandardMaterial("groundTexture", scene);
    var materialPanneau = new BABYLON.StandardMaterial("panneauTexture", scene);
    var materialDome = new BABYLON.StandardMaterial("panneauTexture", scene);
    var materialPillar = new BABYLON.StandardMaterial("pillarTexture", scene);
    materialPillar.diffuseTexture = new BABYLON.Texture("asset/images/futur.png", scene);
    materialPillar.diffuseTexture.uScale = 1.0;
    materialPillar.diffuseTexture.vScale = 1.0;
    materialDome.diffuseTexture = new BABYLON.Texture("asset/images/dome.jpg", scene);
    materialDome.diffuseTexture.uScale = 10.0;
    materialDome.diffuseTexture.vScale = 10.0;
    materialGround.diffuseTexture = new BABYLON.Texture("asset/images/desert.jpeg", scene);
    materialGround.diffuseTexture.uScale = 5.0;
    materialGround.diffuseTexture.vScale = 5.0;
    materialPanneau.diffuseTexture = new BABYLON.Texture("asset/images/panneau.jpeg", scene);
    materialPanneau.diffuseTexture.uScale = 3.0;
    materialPanneau.diffuseTexture.vScale = 3.0;
    // Création de notre lumière principale
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    var ground = BABYLON.Mesh.CreateGround("ground", 20, 20, 1, scene);
    ground.scaling = new BABYLON.Vector3(20,10,30);

    var generateur1 = new BABYLON.Mesh.CreateBox("gene", 3, scene, true, 0);
    generateur1.scaling = new BABYLON.Vector3(5,0.1,2);
    generateur1.position = new BABYLON.Vector3(-3,((3/2)*generateur1.scaling.y),-3);
    var generateur2 = generateur1.clone("gene2");
    generateur2.position = new BABYLON.Vector3(-20,((3/2)*generateur2.scaling.y),-3);
    var generateur3 = generateur1.clone("gene3");
    generateur3.position = new BABYLON.Vector3(-20,((3/2)*generateur3.scaling.y),-10);
    var generateur4 = generateur1.clone("gene4");
    generateur4.position = new BABYLON.Vector3(-3,((3/2)*generateur4.scaling.y),-10);
    generateur1.scaling = new BABYLON.Vector3(5,0.1,2);

    var dome = new BABYLON.Mesh.CreateSphere("dome", 10, 50, scene);
    dome.position= new BABYLON.Vector3(-10, -10, 35);

    var pilon1 = new BABYLON.Mesh.CreateCylinder("pillar1", 10,0.5,2,100,5, scene, true, 0);
    pilon1.position = new BABYLON.Vector3(10, ((3/2)*pilon1.scaling.y), 15);
    var pilon2 = pilon1.clone("pillar2");
    pilon2.position = new BABYLON.Vector3(10, ((3/2)*pilon2.scaling.y), 55);
    var pilon3 = pilon1.clone("pillar3");
    pilon3.position = new BABYLON.Vector3(-30, ((3/2)*pilon3.scaling.y), 55);
    var pilon4 = pilon1.clone("pillar4");
    pilon4.position = new BABYLON.Vector3(-30, ((3/2)*pilon4.scaling.y), 15);

    var base = new BABYLON.Mesh.CreateCylinder("base", 10, 2 , 2, 100, 5, scene, true, 0);
    base.rotation.x = (Math.PI * 90) / 180;
    base.position = new BABYLON.Vector3( -40, ((3/2)*base.scaling.y), -25)
    var base2 = base.clone("base2");
    base2.position = new BABYLON.Vector3( -35, ((3/2)*base.scaling.y), -25)
    generateur1.material = materialPanneau;
    generateur2.material = materialPanneau;
    generateur3.material = materialPanneau;
    generateur4.material = materialPanneau;
    dome.material = materialDome;
    pilon1.material = materialPillar;
    pilon2.material = materialPillar;
    pilon3.material = materialPillar;
    pilon4.material = materialPillar;
    ground.material = materialGround;
};

    //var ground = BABYLON.Mesh.CreateGround("ground1", 10, 5, 1, scene);
    //var box = BABYLON.Mesh.CreateBox("cube1", 5, scene, true, -30);
    //var sphere = BABYLON.Mesh.CreateSphere("sphere1", 16, 2, scene);
    //var cylinder = BABYLON.Mesh.CreateCylinder("cylinder1", 10, 5, 7, 10, 5, scene, true, 0);
