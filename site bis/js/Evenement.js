class Evenement {

	constructor (unNom, idImpact, valeurImpact) {
		this.nom = unNom;
		this.estActif = false;

		this.impact = idImpact; //tableau d'entiers
		this.valImpact = valeurImpact;
	}

	agir(base) {
		var baseReturn = new Base();
		baseReturn.energie = base.energie;
		baseReturn.materiel = base.materiel;
		baseReturn.securite = base.securite;
		baseReturn.eau = base.eau;
		baseReturn.alimentation = base.alimentation;
		baseReturn.drone = base.drone;
		baseReturn.communication = base.communication;
		baseReturn.moral = base.moral;
		baseReturn.sante = base.sante;

		for (var i = 0; i<this.impact.length; i++) {
			switch (this.impact[i]) {
				case 0:
					console.log("energie");
					baseReturn.energie += this.valImpact[i];
					break;
				case 1:
					console.log("materiel");
					baseReturn.materiel += this.valImpact[i];
					break;
				case 2:
					console.log("securite");
					baseReturn.securite += this.valImpact[i];
					break;
				case 3:
					console.log("eau");
					baseReturn.eau += this.valImpact[i];
					break;
				case 4:
					console.log("alimentation");
					baseReturn.alimentation += this.valImpact[i];
					break;
				case 5:
					console.log("drone"); //ON/OFF
					baseReturn.drone = this.valImpact[i];
					break;
				case 6:
					console.log("communication"); //ON/OFF
					baseReturn.communication = this.valImpact[i];
					break;
				case 7:
					console.log("moral");
					baseReturn.moral += this.valImpact[i];
					break;
				case 8:
					console.log("sante");
					baseReturn.sante += this.valImpact[i];
			}
			console.log(this.valImpact[i]);
		}
		this.estActif = true;
		return baseReturn;
	}
}