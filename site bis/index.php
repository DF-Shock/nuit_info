<!DOCTYPE html>

<html>
	<?php
		$servername = "127.0.0.1";
		$username = "user";
		$password = "mdp";
		$dbname = "lanuitdelinfo";

		try { $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password); } catch(Exception $e) { die('Erreur : '.$e->getMessage()); }

		$reponse = $conn->query('SELECT * FROM valuedata');
		while($donnees = $reponse->fetch()) {
			if($donnees['id']==1) {
				$energie = $donnees['value'];
			}
			if($donnees['id']==2) {
				$sante = $donnees['value'];
			}
			if($donnees['id']==3) {
				$eaualimentation = $donnees['value'];
			}
			if($donnees['id']==4) {
				$materiel = $donnees['value'];
			}
		}
		$reponse->closeCursor();
    ?>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Vivre</title>

		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" type="text/css" href="css/condition.css">

	</head>
	<body class="HolyGrail">
		<div class="HolyGrail-body">
			<main class="HolyGrail-content">
				<img src="./image/namibBase.jpg" alt="Base de Namib" class="base">
			</main>

			<nav class="HolyGrail-nav">
				<div class="casenav" id="energie">
					<div class="firstcase">
						<img class="imgcase" src="image/energie.png">
						<div class="textcase">
							<h1>Energie</h1>
							<p><?php echo $energie;?>%</p>
						</div>
					</div>
					<div class="secondcase">
						<img class="imgcase" src="image/energie.png">
					</div>
				</div>
				<div class="casenav" id="sante">
					<div class="firstcase">
						<img class="imgcase" src="image/sante.png">
						<div class="textcase">
							<h1>Santé</h1>
							<p><?php echo $sante;?>%</p>
						</div>
					</div>
				</div>
				<div class="casenav" id="eaualimentation">
					<div class="firstcase">
						<img class="imgcase" src="image/eaualimentation.png">
						<div class="textcase">
							<h1 class="font15">Eau &amp; Alimentation</h1>
							<p><?php echo $eaualimentation;?>%</p>
						</div>
					</div>
				</div>
				<div class="casenav" id="materiel">
					<div class="firstcase">
						<img class="imgcase" src="image/materiel.png">
						<div class="textcase">
							<h1>Materiel</h1>
							<p><?php echo $materiel;?>%</p>
						</div>
					</div>
				</div>
			</nav>

    		<aside class="HolyGrail-ads">
				<div class="casenav" id="drone">
					<img class="imgcase" src="image/drone.png">
					<div class="textcase">
						<h1>Drone</h1>
						<p>off</p>
					</div>
				</div>
				<div class="casenav" id="habitat">
					<img class="imgcase" src="image/habitat.png">
					<div class="textcase">
						<h1>Habitat</h1>
						<p>RAS</p>
					</div>
				</div>
				<div class="casenav" id="securite">
					<img class="imgcase" src="image/securite.png">
					<div class="textcase">
						<h1>Sécurité</h1>
						<p>RAS</p>
					</div>
				</div>
				<div class="casenav" id="communication">
					<img class="imgcase" src="image/communication.png">
					<div class="textcase">
						<h1 class="font15">Communication</h1>
						<p>on</p>
					</div>
				</div>
    		</aside>
		</div>
	</body>
</html>